import { useState } from 'react';
import { StyleSheet, Text, View , Button,ScrollView} from 'react-native';

export default function Lap({time,list,setList}){
    
    const [i,setI]=useState(1);
    function lapHandler(time){
      setList(prevlist=>{
        setI(prevI=>prevI+1);
        prevlist.push({lap:`Lap- ${i}`,item:  ("0"+Math.floor((time/60000)%60)).slice(-2)+":"+("0"+Math.floor((time/1000)%60)).slice(-2)+":"+("0"+Math.floor((time/10)%100)).slice(-2)  ,key:Math.random().toString()});
        console.log(prevlist);
        return prevlist;
      })
     
    }
    return(
         <View>
             <Button title='Lap' onPress={()=>{
              lapHandler(time);
              }
              }></Button> 

              <View key={list.key} style={styles.laps}>
             { list.map((it)=>(
                  <Text style={styles.lap}>{it.lap+"         "+it.item}</Text>
              ))
            }
          </View>
        </View>
        

    )
}
const styles = StyleSheet.create({
  laps: {
    padding: 50,
    justifyContent: 'center',
  },
  lap:{
    fontSize: 20,
},})