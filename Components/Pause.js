import { StyleSheet, Text, View , Button} from 'react-native';

export default function Pause({setPower,power}){
    return(
        (power==true)&&
         (<View>
             <Button title='Pause' onPress={()=>setPower(false)}></Button> 
        </View>)
    )
}