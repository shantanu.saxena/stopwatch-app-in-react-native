import { setStatusBarStyle } from 'expo-status-bar';
import { StyleSheet, Text, View , Button} from 'react-native';

export default function Reset({setPower,setTime,time,setList}){
    return(
        (time!=0)&&
         (<View>
             <Button title='Reset' onPress={()=>
                {
                    setPower(false);
                    setTime(0);
                    setList([{lap:"S.No.",item:"Time Stamp",key:"0"}]);
                
                }}></Button> 
        </View>)
    )
}