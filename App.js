
import { useState,useEffect } from 'react';
import { StyleSheet, Text, View , Button} from 'react-native';
import Start from './Components/Start'
import Pause from './Components/Pause'
import Resume from './Components/Resume'
import Lap from './Components/Lap'
import Reset from './Components/Reset'


export default function App() {
const [power,setPower] = useState(false);
const [time,setTime] = useState(0);
const[list,setList]=useState([{lap:"S.No.",item:"Time Stamp",key:"0"}]);

    useEffect(()=>{
      let Interval=null;
        if(power==true)
        {
          
            Interval=setInterval(()=>{
            setTime(prevTime=>prevTime+10)
            },10) 
        }
        else
        {
            clearInterval(Interval);
        }
        return ()=>clearInterval(Interval);

        },[power])

  return (
    <View style={styles.container}>
      <View > 
        <Text style={styles.timer}>{("0"+Math.floor((time/60000)%60)).slice(-2)+":"+("0"+Math.floor((time/1000)%60)).slice(-2)+":"+("0"+Math.floor((time/10)%100)).slice(-2)}</Text>
    </View>
    <View style={styles.buttons}>
      <Start  setPower={setPower} power={power}/>
      <Pause setPower={setPower} power={power}/>
      <Resume setPower={setPower} power={power} time={time} />
      
      <Reset setPower={setPower} setTime={setTime} time={time} setList={setList}/>
      <Lap time={time} list={list} setList={setList}/>
    </View>
    </View>
    
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  timer:{
    fontSize: 50,
    padding: 50
  },
 
});

